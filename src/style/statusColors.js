import msColors from './mindsphereColors';

export default {
  NORMAL: msColors.functionalGreen,
  UNSOLVED: msColors.functionalRed,
  PENDING: msColors.functionalYellow,
  FIXED: msColors.functionalGreen,
}