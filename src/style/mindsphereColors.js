export default {
  coolBlue: '#009EFF',
  stateBlue100: '#006FE6',
  stateBlue20: '#CCE2FA',
  gray100: '#F0F0F0',
  gray250: '#BEBEBE',
  functionalRed: '#F62447',
  functionalYellow: '#FFC800',
  functionalGreen: '#65C728',
  contextGreen50: '#E6EED1',
  contextGreen600: '#5E6919',
  contextRed: '#FCD3D2',
  contextRed500: '#811211'
};