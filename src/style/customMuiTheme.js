import { createMuiTheme } from '@material-ui/core/styles';
import msColors from '../style/mindsphereColors';

const customTheme = createMuiTheme({
  overrides: {
    MuiTable: {
      root: {
        border: '1px solid ' + msColors.gray250
      }
    },
    MuiTableCell: {
      root: {
        borderBottom: '1px solid ' + msColors.gray250
      } 
    }
  },
  palette: {
    primary: {
      main: msColors.stateBlue100,
    },
    secondary: {
      main: msColors.stateBlue20
    },
    error: {
      main: msColors.functionalRed
    },
    background: {
      paper: msColors.gray100
    }
  },
});

export default customTheme;