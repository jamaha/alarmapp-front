import io from 'socket.io-client';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { STATUS_EVENT } from './store/status';
import STATUS from './store/status';
import { setAlert } from './store/alert';

const socket = io(process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : '/', {
  path: '/alarmApi/socket'
});

export default function SocketEventWrapper({ children }) {
  const dispatch = useDispatch();
  useEffect(() => {
    socket.on('message', (data) => {
      console.log('message', data);
      if (data.status) {
        dispatch({ type: STATUS_EVENT, data });
        if (data.status === STATUS.UNSOLVED.name) {
          dispatch(setAlert(data.assetName, data.aspectName));
        }
      }
    });
  // eslint-disable-next-line
  },[]);
  return (children)
}
