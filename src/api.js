import axios from 'axios';
import qs from 'qs';

const api = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? 'http://localhost:3000/alarmApi' : '/alarmApi',
  timeout: 10000
});

const storageId = "8cedf1d4cca24251bea59d1b91cb1d3c";

export const getAssets = async () => {
  try {
    const response = await api.get('/assets');
    return response.data.content;
  } catch (error) {
    console.warn('Error on getAssets', error);
  }
}

export const getAspects = async (typeId,assetId) => {
  try {
    const response = await api.get('/assettype?id=' + typeId + '&eid=' + assetId);
    return response.data.content;
  } catch (error) {
    console.warn('Error on getAspects', error);
  }
}

export const getVariables = async (assetId, assetTypeId, aspectName) => {
  try {
    const response = await api.get('/variables?id=' + assetTypeId + '&eid=' + assetId + '&pName='+aspectName);
    return response.data.content;
  } catch (error) {
    console.warn('Error on getAspects', error);
  }
}

export const getGroups = async () => {
  try {
    const response = await api.get('/file?eid='+storageId+'&filepath=groups');
    return response.data;
  } catch (error) {
    console.warn('Error on getGroups', error);
  }
}

export const getLogs = async (name) => {
  try {
    const response = await api.get('/file?eid='+storageId+'&filepath=logfile'+name);
    return response.data;
  } catch (error) {
    console.warn('Error on getLogs', error);
  }
}

export const saveThreshold = async (values) => {
  try {
    const response = await api.post('/file/create/threshold',values);
    return {
        success: response.data === 'success',
        message: response.data
      }
  } catch (error) {
    console.warn('Error on saveThreshold', error);
  }
}

export const deleteThreshold = async (id) => {
  try {
    const response = await api.post('/file/delete/threshold',id);
    return {
        success: response.data === 'success',
        message: response.data
      }
  } catch (error) {
    console.warn('Error on saveThreshold', error);
  }
}

export const getThresholds = async (assetId, aspectName,variable) => {
  try {
    console.log("get thresholds "+assetId+" "+aspectName+" "+variable);
    const response = await api.get('/thresholds?eid='+assetId+'&aName='+aspectName+'&vName='+variable);
    console.log(response.data);
    return response.data;
  } catch (error) {
    console.warn('Error on getThresholds', error);
  }
}

export const submitLogMessage = async (values,fixedValues) => {
  try {
    console.log(fixedValues);
    console.log(values);
    const response = await api.post('/file/create/logfile',values);
    if(response.data === "success"){
      await api.post('/status/fixed',fixedValues);
    }
    console.log(response.data);
    return response.data;
  } catch (error) {
    console.warn('Error on submitLogMessage', error);
  }
}

export const setStatusToPending = async (values) => {
  try {
    await api.post('/status/pending',values);
  } catch (error) {
    console.warn('Error on setStatusToPending', error);
  }
}

export const setStatusToUnsolved = async (values) => {
  try {
    await api.post('/status/unsolved',values);
  } catch (error) {
    console.warn('Error on setStatusToUnsolved', error);
  }
}

export const getTimeseries = async (query) => {
  try {
    const response = await api.get('/timeseries/aggregate?' + qs.stringify(query));
    return response.data;
  } catch (error) {
    console.warn('Error on getTimeseries', error);
  }
}

export const getAllStatuses = async () => {
  try {
    const response = await api.get('/statuses');
    return response.data;
  } catch (error) {
    console.warn('Error on getAllStatuses', error);
  }
}

export const test = async () => {
  const response = await api.get();
  return response.data;
}
