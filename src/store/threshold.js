import { getThresholds } from '../api';

const defaultState = {
  ownerAspectName: '',
  ownerVariableName: '',
  list: [],
}

const SET_THRESHLOD_LIST = 'SET_THRESHOLD_LIST';

export const fetchThresholds = (assetId, aspectName, variableName) => async dispatch => {
  const thresholds = await getThresholds(assetId, aspectName, variableName);
  dispatch({
    type: SET_THRESHLOD_LIST,
    list: thresholds || [],
    ownerAspectName: aspectName,
    ownerVariableName: variableName
  });
}

export default function aspectReducer(state = defaultState, action) {
  const { type, ...data } = action;
  switch (type) {
    case SET_THRESHLOD_LIST:
      return { ...state, ...data }
    default:
      return state
  }
}