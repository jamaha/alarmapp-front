import React from 'react';
import thunk from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';

import assetReducer from './asset';
import aspectReducer from './aspect';
import variableReducer from './variable';
import thresholdReducer from './threshold';
import alertReducer from './alert';

const rootReducer = combineReducers({
  asset: assetReducer,
  aspect: aspectReducer,
  variable: variableReducer,
  threshold: thresholdReducer,
  alert: alertReducer
});

const composedEnhancers = composeWithDevTools(applyMiddleware(thunk));
const store = createStore(rootReducer, composedEnhancers);

export default function ReduxProvider({ children }) {
  return (
    <Provider store={store}>
      {children}
    </Provider>
  )
}