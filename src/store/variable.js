import { getVariables } from '../api';
import { STATUS_EVENT } from './status';

const defaultState = {
  ownerAssetId: '',
  ownerAspectName: '',
  selected: {},
  list: [],
}

const SET_VARIABLE_LIST = 'SET_VARIABLE_LIST';
const SET_SELECTED_VARIABLE = 'SET_SELECTED_VARIABLE';

export const fetchVariables = (assetId, assetTypeId, aspectName) => async dispatch => {
  const variables = await getVariables(assetId, assetTypeId, aspectName);
  dispatch({
    type: SET_VARIABLE_LIST,
    list: variables || [],
    ownerAspectName: aspectName,
    ownerAssetId: assetId
  });
}

export const setSelectedVariable = (variable) => {
  return { type: SET_SELECTED_VARIABLE, selected: variable || {} }
}

export default function aspectReducer(state = defaultState, action) {
  const { type, ...data } = action;
  switch (type) {
    case SET_VARIABLE_LIST:
      return { ...state, ...data }
    case SET_SELECTED_VARIABLE:
      return { ...state, selected: action.selected }
    case STATUS_EVENT: {
      const { assetId, aspectName, status } = data.data;
      if (assetId !== state.ownerAssetId || aspectName !== state.ownerAspectName) return state;
      return {
        ...state,
        list: state.list.map(variable =>
          variable.aspectName === aspectName ? { ...variable, status } : variable
        )
      }
    }
    default:
      return state
  }
}