const defaultState = {
  aspectName: '',
  assetName: ''
}

export const SET_ALERT = 'SET_ALERT';

export const setAlert = (assetName, aspectName) => {
  return { type: SET_ALERT, assetName, aspectName }
}

export const clearAlert = () => {
  return { type: SET_ALERT, aspectName: '', assetName: '' }
}

export default function aspectReducer(state = defaultState, action) {
  const { type, ...data } = action;
  switch (type) {
    case SET_ALERT:
      return { ...data }
    default:
      return state
  }
}