export default {
  NORMAL:{
    name: 'NORMAL',
    level: 0
  },
  UNSOLVED:{
      name: 'UNSOLVED',
      level: 4
  },
  PENDING: {
      name: 'PENDING',
      level: 2
  },
  FIXED: {
      name: 'FIXED',
      level: 1
  },
}

export const STATUS_EVENT = 'STATUS_EVENT';