import { getAssets } from '../api';

const defaultState = {
  selected: {},
  list: [],
}

const SET_ASSET_LIST = 'SET_ASSET_LIST';
const SET_SELECTED_ASSET = 'SET_SELECTED_ASSET';

export const fetchAssets = () => async dispatch => {
  const assets = await getAssets();
  dispatch(setAssetList(assets));
}

export const setSelectedAsset = (asset) => {
  return { type: SET_SELECTED_ASSET, selected: asset || {} }
}

const setAssetList = (assetList) => {
  return { type: SET_ASSET_LIST, list: assetList || [] }
}

export default function assetReducer(state = defaultState, action) {
  switch (action.type) {
    case SET_ASSET_LIST:
      return { ...state, list: action.list }
    case SET_SELECTED_ASSET:
      return { ...state, selected: action.selected }
    default:
      return state
  }
}