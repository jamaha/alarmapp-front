import { getAspects } from '../api';
import { STATUS_EVENT } from './status';

const defaultState = {
  ownerAssetId: '',
  selected: {},
  list: [],
}

const SET_ASPECT_LIST = 'SET_ASPECT_LIST';
const SET_SELECTED_ASPECT = 'SET_SELECTED_ASPECT';

export const fetchAspects = (assetId, assetTypeId) => async (dispatch, getState) => {
  const aspects = await getAspects(assetTypeId, assetId);
  dispatch({
    type: SET_ASPECT_LIST,
    list: aspects || [],
    ownerAssetId: assetId
  });
}

export const setSelectedAspect = (aspect) => {
  return { type: SET_SELECTED_ASPECT, selected: aspect }
}

export default function aspectReducer(state = defaultState, action) {
  const { type, ...data } = action;
  switch (type) {
    case SET_ASPECT_LIST: {
      return { ...state, ...data }
    }
    case SET_SELECTED_ASPECT:
      return { ...state, selected: data.selected }
    case STATUS_EVENT: {
      const { assetId, aspectName, status } = data.data;
      if (assetId !== state.ownerAssetId) return state;
      return {
        ...state,
        list: state.list.map(aspect =>
          aspect.name === aspectName ? { ...aspect, status } : aspect
        ),
        selected: state.selected.name === aspectName
        ?
        { ...state.selected, status }
        :
        state.selected
      }
    }
    default:
      return state
  }
}