import React, { useState } from 'react';
import Container from '@material-ui/core/Container';
import { ThemeProvider } from '@material-ui/core/styles';

import AssetsView from './views/Assets';
import AspectsView from './views/Aspects';
import VariablesView from './views/Variables';
import VariableDetails from './views/VariableDetails';
import ThresholdsView from './views/Thresholds';
import ThresholdView from './views/Threshold';
import LogsView from './views/Logs';
import SubmitLogView from './views/SubmitLog';
import BackButton from './components/BackButton';
import ReduxProvider from './store/ReduxProvider';
import SocketEventWrapper from './SocketEventWrapper';

import theme from './style/customMuiTheme';
import AlertMessage from './components/AlertMessage';

const VIEWS = {
  ASSETS: 1,
  ASPECTS: 2,
  VARIABLES: 3,
  VARIABLE_DETAILS: 4,
  THRESHOLD_LIST:5,
  THRESHOLD: 6,
  LOGS: 7,
  SUBMITLOG:8,
};

const PREVIOUS_VIEW = {
  [VIEWS.ASPECTS]: VIEWS.ASSETS,
  [VIEWS.VARIABLES]: VIEWS.ASPECTS,
  [VIEWS.VARIABLE_DETAILS]: VIEWS.VARIABLES,
  [VIEWS.THRESHOLD_LIST]: VIEWS.VARIABLE_DETAILS,
  [VIEWS.THRESHOLD]: VIEWS.THRESHOLD_LIST,
  [VIEWS.LOGS]: VIEWS.VARIABLES,
  [VIEWS.SUBMITLOG]: VIEWS.VARIABLES
}

function App() {
  const [view, setView] = useState(VIEWS.ASSETS);
  const [threshold, setThreshold] = useState(null);

  function openThresholdView(threshold){
    setThreshold(threshold);
    setView(VIEWS.THRESHOLD);
  }

  function onBack() {
    const previousView = PREVIOUS_VIEW[view];
    if (previousView) {
      setView(previousView);
    }
  }
  
  return (
    <ReduxProvider>
      <SocketEventWrapper>
        <ThemeProvider theme={theme}>
          <Container maxWidth="sm" style={{
            position: 'relative',
            paddingTop: '1em',
            height: '100%'
          }}>
            <div style={{ overflowY: 'auto', height: '100%' }}>
              {(() => {
                switch (view) {
                  case VIEWS.ASSETS:
                    return <AssetsView onSelect={() => setView(VIEWS.ASPECTS)} />
                  case VIEWS.ASPECTS:
                    return <AspectsView
                      onSelect={() => setView(VIEWS.VARIABLES)}
                    />
                  case VIEWS.VARIABLES:
                    return <VariablesView
                      onSelect={() => setView(VIEWS.VARIABLE_DETAILS)}
                      onFixedButtonClick={() => setView(VIEWS.SUBMITLOG)}
                      onLogButtonClick={() => setView(VIEWS.LOGS)}
                    />
                  case VIEWS.VARIABLE_DETAILS:
                    return <VariableDetails
                      onThresholdsButtonClick={() => setView(VIEWS.THRESHOLD_LIST)}
                    />
                  case VIEWS.THRESHOLD_LIST:
                    return <ThresholdsView
                      onCreateThreshold={() => openThresholdView(null)}
                      onThresholdDeleted={() => setView(VIEWS.THRESHOLD_LIST)}
                      onSelect={openThresholdView}
                    />
                  case VIEWS.THRESHOLD:
                    return <ThresholdView
                      threshold={threshold}
                      onSaveDone={() => setView(VIEWS.THRESHOLD_LIST)}
                    />
                  case VIEWS.LOGS:
                    return <LogsView 
                      onBack={() => setView(VIEWS.VARIABLES)}
                    />
                  case VIEWS.SUBMITLOG:
                    return <SubmitLogView 
                      onLogCreated={() => setView(VIEWS.VARIABLES)}
                    />
                  default:
                    return <p>Unknown view {view}</p>
                }
              })()}
            </div>
            {view !== VIEWS.ASSETS &&
              <BackButton onClick={onBack} />
            }
          </Container>
          <AlertMessage />
        </ThemeProvider>
      </SocketEventWrapper>
    </ReduxProvider>
  );
}

export default App;
