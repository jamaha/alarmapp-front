export const STATUS = {
  UNSOLVED: 'UNSOLVED',
  PENDING: 'PENDING',
  FIXED: 'FIXED',
  NORMAL: 'NORMAL'
};

const level = [
  'NORMAL',
  'FIXED',
  'PENDING',
  'UNSOLVED',
]

export function getAssetLevelStatus(aspects) {
  if (!aspects) return 'No state';
  let status = 'No state';
  for (const aspect of Object.keys(aspects)) {
    const aspectStatus = getAspectLevelStatus(aspects[aspect]);
    if (level.indexOf(aspectStatus) > level.indexOf(status)) {
      status = aspectStatus;
    }
  }
  return status;
}

export function getAspectLevelStatus(variables) {
  if (!variables) return 'No state';
  let status = 'No state';
  for (const variable of Object.keys(variables)) {
    const variableStatus = getVariableLevelStatus(variables[variable]);
    console.log('var status', variable, variableStatus);
    if (level.indexOf(variableStatus) > level.indexOf(status)) {
      status = variableStatus;
    }
  }
  return status;
} 

export function getVariableLevelStatus(thresholds) {
  if (!thresholds) return 'No state';
  let status = 'No state';
  for (const tresh of Object.keys(thresholds)) {
    if (level.indexOf(thresholds[tresh].status) > level.indexOf(status)) {
      status = thresholds[tresh].status;
    }
  }
  return status
}
