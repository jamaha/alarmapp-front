import React from 'react';
import Typography from '@material-ui/core/Typography';

export const ViewTitle = ({children, ...props}) => {
  return (
    <Typography
      variant='h4'
      gutterBottom
      style={{ marginLeft: '0.5em', fontWeight: 'bold' }}
    >
      {children}
    </Typography>
  )
}