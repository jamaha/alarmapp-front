import React from 'react';
import PropTypes from 'prop-types';

const ChartAxisTick = (props) => {
    const {x, y, payload} = props;
    const date = new Date(payload.value);
    
  return (
    <g transform={`translate(${x},${y})`}>
      <text x={55} y={0} dy={16} textAnchor="end" fill="#666">
        {date.toLocaleDateString()}
      </text>
    </g>
  );
};

ChartAxisTick.propTypes = {
  x: PropTypes.number,
  y: PropTypes.number,
  payload: PropTypes.object
}

export default ChartAxisTick;