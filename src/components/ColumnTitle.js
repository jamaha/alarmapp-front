import React from 'react';
import Typography from '@material-ui/core/Typography';

export const ColumnTitle = ({children, ...props}) => {
  return (
    <Typography style={{ fontWeight: 'bold' }} >
      {children}
    </Typography>
  )
}