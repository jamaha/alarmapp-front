import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Snackbar, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import { clearAlert } from '../store/alert';
import msColors from '../style/mindsphereColors';

function AlertMessage() {
  const dispatch = useDispatch();
  const assetName = useSelector(state => state.alert.assetName);
  const aspectName = useSelector(state => state.alert.aspectName);

  return (
    <Snackbar
      open={!!(assetName && aspectName)}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      message={`${aspectName} of ${assetName} gone critical`}
      style={{ zIndex: 10002, }}
      ContentProps={{style: {
        backgroundColor: msColors.contextRed,
        color: msColors.contextRed500
      }}}
      action={[
        <IconButton
          key='close'
          color='inherit'
          onClick={() => dispatch(clearAlert())}
        >
          <CloseIcon />
        </IconButton>
      ]}
    />
  );
}

export default AlertMessage;