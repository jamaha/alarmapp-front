import React from 'react';
import { Paper } from '@material-ui/core';
import statusColors from '../style/statusColors';

export default function StatusLabel({ status }) {
  return (
    <Paper
      style={{
        background: statusColors[status],
        display: 'inline-block',
        padding: '3px 6px',
        color: 'white',
        fontWeight: 'bold'
      }}
    >
      {status}
    </Paper>
  )
}