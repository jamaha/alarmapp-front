import React from 'react';
import Fab from '@material-ui/core/Fab';
import { useTheme } from '@material-ui/core/styles';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

export default function BackButton(props) {
  const theme = useTheme();
  return (
    <Fab 
      //variant='contained'
      color='primary'
      style={{
        position: 'absolute', 
        bottom: theme.spacing(3),
        right: theme.spacing(6)
      }}
      {...props}
    >
      <ArrowBackIcon style={{ color: '#fff' }} />
    </Fab >
  )
}