import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { Table, TableHead, TableRow, TableCell, TableBody, Paper, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import * as api from '../api';
import { fetchVariables, setSelectedVariable } from '../store/variable';
import { ViewTitle } from '../components/ViewTitle';
import { ColumnTitle } from '../components/ColumnTitle';
import StatusLabel from '../components/StatusLabel';

const useStyles = makeStyles(theme => ({
  textField: {
    width: '100%'
  },
  formControl: {
    width: '10em'
  },
  buttonsLayout: {
    '& > *': {
      margin: theme.spacing(2, 1, 0, 1),
    },
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }
}));

function VariablesView(props) {
  const dispatch = useDispatch();
  const variables = useSelector(state => state.variable.list);
  const asset = useSelector(state => state.asset.selected);
  const aspect = useSelector(state => state.aspect.selected);
  const ownerAspectName = useSelector(state => state.variable.ownerAspectName);
  const classes = useStyles();

  useEffect(() => {
    if (ownerAspectName !== aspect.name) {
      dispatch(fetchVariables(asset.assetId, asset.typeId, aspect.name));
    }
  }, [variables, asset, aspect, dispatch, ownerAspectName]);

  function onSelect(variable) {
    dispatch(setSelectedVariable(variable));
    props.onSelect();
  }

  function accept() {
    const values = {
      name: "empty",
      entityId: asset.assetId,
      aspectName: aspect.name
    }
    api.setStatusToPending(values);
  }

  function unsolved() {
    const values = {
      name: "empty",
      entityId: asset.assetId,
      aspectName: aspect.name
    }
    api.setStatusToUnsolved(values);
  }

  return (
    <>
      <ViewTitle>Variables of {aspect.name}</ViewTitle>
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <ColumnTitle>
                  Variable
                </ColumnTitle>
              </TableCell>
              <TableCell>
                <ColumnTitle>
                  Status
                </ColumnTitle>
              </TableCell>
              <TableCell>
                <ColumnTitle>
                  Unit
                </ColumnTitle>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {variables.map(variable => (
              <TableRow
                key={variable.name}
                onClick={() => onSelect(variable)}
                hover
              >
                <TableCell>{variable.name}</TableCell>
                <TableCell>
                  <StatusLabel status={variable.status}/>
                </TableCell>
                <TableCell>{variable.value} {variable.unit}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
      <div className={classes.buttonsLayout}>
        <Button
          variant='outlined'
          size='large'
          color="primary"
          style={acceptEnabled.find(s => s === aspect.status) ? {} : { display: 'none' }}
          onClick={() => accept()}
        >
          ACCEPT
        </Button>
        <Button
          variant='outlined'
          size='large'
          color="primary"
          style={unsolvedEnabled.find(s => s === aspect.status) ? {} : { display: 'none' }}
          onClick={() => unsolved()}
        >
          UNSOLVED
        </Button>
        <Button
          color="primary"
          size='large'
          variant='outlined'
          style={fixedEnabled.find(s => s === aspect.status) ? {} : { display: 'none' }}
          onClick={() => props.onFixedButtonClick()}
        >
          FIXED
        </Button>
        <Button
          size='large'
          onClick={props.onLogButtonClick}
        >
          Logs
        </Button>
      </div>
      <div className={classes.buttonsLayout}>
      </div>
    </>
  )
}

const acceptEnabled = [
  'UNSOLVED'
];
const unsolvedEnabled = [
  'FIXED',
  'PENDING'
]
const fixedEnabled = [
  'UNSOLVED',
  'PENDING'
]

VariablesView.propTypes = {
  onSelect: PropTypes.func,
  onLogButtonClick: PropTypes.func,
  onFixedButtonClick: PropTypes.func
}

export default VariablesView;
