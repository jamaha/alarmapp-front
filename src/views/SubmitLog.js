import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { makeStyles, TextField, Button, Snackbar, Dialog, DialogTitle } from '@material-ui/core';

import * as api from '../api';
import { ViewTitle } from '../components/ViewTitle';
import msColors from '../style/mindsphereColors';

const useStyles = makeStyles(theme => ({
  form: {
    display: 'flex',
    flexDirection: 'column'
  },
  button: {
    margin: theme.spacing(3),
    alignSelf: 'center'
  }
}));

let values = {
  name: "",
  entityId: "",
  aspectName: "",
  desc: "",
  date: ""
}

let fixedValues = {
  name: "empty",
  entityId: "",
  aspectName: ""
}

export default function SubmitLogView({ onLogCreated }) {
  const classes = useStyles();
  const assetId = useSelector(state => state.asset.selected.assetId);
  const aspect = useSelector(state => state.aspect.selected);
  const [snackbarText, setSnackbarText] = useState('');
  const [submitDone, setSubmitDone] = useState(false);

  async function submitLog() {
    const result = await api.submitLogMessage(values, fixedValues);
    if (result === "success") {
      onSubmitSuccess();
    } else {
      onSubmitFailure(result);
    }
  }

  function onSubmitSuccess() {
    setSubmitDone(true);
    setTimeout(onLogCreated, 1800);
  }

  function onSubmitFailure(message) {
    setSnackbarText(message);
    setTimeout(() => setSnackbarText(''), 5000);
  }

  values.entityId = assetId;
  values.aspectName = aspect.name;

  fixedValues.entityId = assetId;
  fixedValues.aspectName = aspect.name;

  return (
    <>
      <ViewTitle>Submit Solution</ViewTitle>
      <form className={classes.form} noValidate autoComplete="off">
        <TextField
          className={classes.textField}
          label="Subject"
          margin="normal"
          variant="outlined"
          defaultValue={values.name}
          onChange={(evt) => { values.name = evt.target.value; console.log(values); }}
        />
        <TextField
          label="Description"
          multiline
          rows="8"
          className={classes.textField}
          margin="normal"
          variant="outlined"
          onChange={(evt) => { values.desc = evt.target.value; }}
        />
        <Button
          variant="outlined"
          color="primary"
          size='large'
          className={classes.button}
          onClick={submitLog}
        >Submit</Button>
        <Snackbar
          ContentProps={{style: {
            backgroundColor: msColors.contextRed,
            color: msColors.contextRed500
          }}}
          open={!!snackbarText}
          autoHideDuration={6000}
          message={<span id="message-id">{snackbarText}</span>}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
        />
        <Dialog
          open={submitDone}
          PaperProps={{ style: {
            backgroundColor: msColors.contextGreen50,
            color: msColors.contextGreen600
          }}}
        >
          <DialogTitle>Log created</DialogTitle>
        </Dialog>
      </form>
    </>
  )
}