import React, { useEffect, useState, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ReferenceLine
} from 'recharts';

import * as api from '../api';
import { fetchThresholds } from '../store/threshold';
import ChartAxisTick from '../components/ChartAxisTick';
import { ViewTitle } from '../components/ViewTitle';
import msColors from '../style/mindsphereColors';

function VariablesView(props) {
  const dispatch = useDispatch();
  const aspectName = useSelector(state => state.aspect.selected.name);
  const assetId = useSelector(state => state.asset.selected.assetId);
  const variableName = useSelector(state => state.variable.selected.name);
  const thresholds = useSelector(state => state.threshold.list);
  const ownerAspectName = useSelector(state => state.threshold.ownerAspectName);
  const ownerVariableName = useSelector(state => state.threshold.ownerVariableName);

  const [timeseriesData, setTimeseriesData] = useState([]);

  const isNewVariable = useCallback(() => {
    return ownerAspectName !== aspectName || ownerVariableName !== variableName;
  }, [ownerAspectName, aspectName, ownerVariableName, variableName]);

  useEffect(() => {
    async function getTimeseriesFromRange() {
      const toDate = new Date();
      toDate.setHours(0);
      toDate.setMinutes(0);
      toDate.setSeconds(0);
      toDate.setMilliseconds(0);
      const fromDate = new Date(toDate.toISOString());
      fromDate.setDate(toDate.getDate() - 5);
      const data = await api.getTimeseries({
        eid: assetId,
        pName: aspectName,
        aName: variableName,
        from: fromDate.toISOString(),
        to: toDate.toISOString(),
        iValue: '3',
        iUnit: 'hour'
      });
      setTimeseriesData(data.map(record => record[variableName]));
    }
    getTimeseriesFromRange();
  }, [assetId, aspectName, variableName]);

  useEffect(() => {
    if (isNewVariable()) {
      dispatch(fetchThresholds(assetId, aspectName, variableName));
    }
  }, [dispatch, assetId, aspectName, variableName, isNewVariable]);

  return (
    <>
      <ViewTitle>{aspectName} {variableName}</ViewTitle>
      {isNewVariable() ? 
        <CircularProgress />
      : drawChart(timeseriesData, thresholds)}
      <div style={{ display: 'flex' }}>
        <Button
          color="primary"
          variant="outlined"
          onClick={props.onThresholdsButtonClick}
          style={{ margin: 'auto' }}
        >
          Thresholds
        </Button>
      </div>
    </>
  )
}

const drawChart = (timeseriesData, thresholds) => (
  <LineChart
    width={500}
    height={300}
    data={timeseriesData}
    margin={{
      top: 5, right: 30, left: 20, bottom: 15,
    }}
  >
    <CartesianGrid strokeDasharray="3 3" />
    <XAxis
      dataKey="firsttime"
      interval='preserveStartEnd'
      tick={<ChartAxisTick />}
    />
    <YAxis />
    <Tooltip />
    <Legend />
    <Line
      type="monotone"
      dataKey="average"
      stroke="#8884d8"
      connectNulls
    />
    {thresholds && mapThresholdsToChartLines(thresholds)}
  </LineChart>
);

function mapThresholdsToChartLines(thresholds) {
  return thresholds.map(threshold => (
    <ReferenceLine
      key={threshold.name}
      y={threshold.threshold}
      label={threshold.name}
      alwaysShow
      stroke={msColors.functionalRed}
    />
  ));
}

VariablesView.propTypes = {
  onThresholdsButtonClick: PropTypes.func
}

export default VariablesView;