import React, { useEffect, useState } from 'react';
import { Table, TableHead, TableRow, TableCell, TableBody, Paper } from '@material-ui/core';

import * as api from '../api';
import { ViewTitle } from '../components/ViewTitle';
import { ColumnTitle } from '../components/ColumnTitle';



export default function GroupView({ onSelect }) {

  const [groups, setGroup] = useState([]);
  useEffect(() => {
    // Async function cannot be passed directly to useEffect
    async function getGroups() {
      const result = await api.getGroups();
      if (Array.isArray(result)) {
        setGroup(result);
      }
    }
    getGroups();
  }, []);

  return (
    <>
      <ViewTitle>Select Group</ViewTitle>
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <ColumnTitle>
                  Groups
                </ColumnTitle>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {groups.map(group => (
              <TableRow
                key={group.name}
                onClick={() => onSelect && onSelect(group)}
                hover
              >
                
                <TableCell>{group.name}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </>
  )
}