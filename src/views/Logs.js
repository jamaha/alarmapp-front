import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Paper, Typography, ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, makeStyles } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import * as api from '../api';
import { ViewTitle } from '../components/ViewTitle';



const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(12),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(10),
    color: theme.palette.text.secondary,
  },
}));


export default function LogView() {
  const assetId = useSelector(state => state.asset.selected.assetId);
  const aspectName = useSelector(state => state.aspect.selected.name);

  const [logs, setLogs] = useState([]);

  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  useEffect(() => {
    async function getLogs() {
      const result = await api.getLogs(assetId + aspectName);
      if (Array.isArray(result)) {
        setLogs(result);
      }
    }
    getLogs();
  }, [aspectName, assetId]);
  
  return (
    <>
      <ViewTitle>Logs</ViewTitle>
      <Paper>
        {logs.map(log => (
          <ExpansionPanel key={log.name + log.date} expanded={expanded === log.date} onChange={handleChange(log.date)}>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls={log.date + "-content"}
              id={log.date + "-header"}
            >
              <Typography className={classes.heading}>{log.name}</Typography>
              <Typography className={classes.secondaryHeading}>{log.date}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>{log.desc}</Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        ))}
      </Paper>
    </>
  )
}