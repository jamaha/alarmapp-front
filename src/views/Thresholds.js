
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Table, TableHead, TableRow, TableCell, TableBody, Paper, Button, Dialog, DialogTitle, Snackbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { ViewTitle } from '../components/ViewTitle';
import { ColumnTitle } from '../components/ColumnTitle';

import { fetchThresholds } from '../store/threshold';
import * as api from '../api';
import msColors from '../style/mindsphereColors';

const useStyles = makeStyles(theme => ({
  textField: {
    width: '100%'
  },
  formControl: {
    width: '10em'
  },
  buttonsLayout: {
    '& > *': {
      margin: theme.spacing(1),
    },
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }
}));

export default function ThresholdsView({ onSelect, onCreateThreshold }) {
  const dispatch = useDispatch();
  const thresholds = useSelector(state => state.threshold.list);
  const classes = useStyles();
  const [snackbarText, setSnackbarText ] = useState('');
  const [submitDone, setSubmitDone] = useState(false);

  async function deleteThreshold(id) {
    const result = await api.deleteThreshold(id);
    console.log('result', result);
    if (result.success) {
      onSubmitSuccess();
    } else {
      onSubmitFailure(result);
    }
  }

  function onSubmitSuccess() {
    dispatch(fetchThresholds());
    setSubmitDone(true);
    setTimeout(() => setSubmitDone(false), 2800);
  }

  function onSubmitFailure(message) {
    setSnackbarText(message);
    setTimeout(() => setSnackbarText(''), 5000);
  }
  return (
    <>
      <ViewTitle>Thresholds</ViewTitle>
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <ColumnTitle>
                  Name
                </ColumnTitle>
              </TableCell>
              <TableCell>
                <ColumnTitle>
                  Type
                </ColumnTitle>
              </TableCell>
              <TableCell>
                <ColumnTitle>
                  Value
                </ColumnTitle>
              </TableCell>
              <TableCell/>
              <TableCell/>
            </TableRow>
          </TableHead>
          <TableBody>
            {thresholds.map(threshold => (
              <TableRow
                key={threshold.name}
                hover
              >
                <TableCell>{threshold.name}</TableCell>
                <TableCell>{threshold.type}</TableCell>
                <TableCell>{threshold.threshold}</TableCell>
                <TableCell>
                  <EditIcon onClick={() => onSelect && onSelect(threshold)}/>
                </TableCell>
                <TableCell onClick={() => deleteThreshold(threshold)}>
                  <DeleteIcon />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
      <div className={classes.buttonsLayout}>
        <Button
          variant='outlined'
          color="primary"
          size='large'
          onClick={onCreateThreshold}
        >
          Create Threshold
          </Button>
      </div>
      <Snackbar
        color='secondary'
        open={!!snackbarText}
        autoHideDuration={5000}
        message={<span id="message-id">{snackbarText}</span>}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      />
      <Dialog open={submitDone}
        PaperProps={{ style: {
          backgroundColor: msColors.contextGreen50,
          color: msColors.contextGreen600
        }}}
      >
        <DialogTitle>Threshold deleted</DialogTitle>
      </Dialog>
    </>
  )
}