import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Table, TableHead, TableRow, TableCell, TableBody, Paper } from '@material-ui/core';

import { fetchAspects, setSelectedAspect } from '../store/aspect';
import { ViewTitle } from '../components/ViewTitle';
import { ColumnTitle } from '../components/ColumnTitle';
import StatusLabel from '../components/StatusLabel';

function AspectView(props) {
  const dispatch = useDispatch();
  const aspects = useSelector(state => state.aspect.list);
  const ownerAssetId = useSelector(state => state.aspect.ownerAssetId);
  const selectedAsset = useSelector(state => state.asset.selected);

  useEffect(() => {
    if (selectedAsset.assetId !== ownerAssetId) {
      dispatch(fetchAspects(selectedAsset.assetId, selectedAsset.typeId));
    }
  }, [aspects, selectedAsset, dispatch, ownerAssetId]);

  function onSelect(aspect) {
    dispatch(setSelectedAspect(aspect));
    props.onSelect();
  }

  return (
    <>
      <ViewTitle>
        {"Aspects of "+ selectedAsset.name}
      </ViewTitle>
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <ColumnTitle>
                  Aspect
                </ColumnTitle>
              </TableCell>
              <TableCell>
                <ColumnTitle>
                  Status
                </ColumnTitle>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {aspects.map(aspect => (
              <TableRow
                key={aspect.name}
                onClick={() => onSelect(aspect)}
                hover
              >
                <TableCell>{aspect.name}</TableCell>
                <TableCell>
                  <StatusLabel status={aspect.status} />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </>
  )
}

AspectView.propTypes = {
  onSelect: PropTypes.func
}

export default AspectView;