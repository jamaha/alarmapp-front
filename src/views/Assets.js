import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { Table, TableHead, TableRow, TableCell, TableBody, Paper } from '@material-ui/core';

import { fetchAssets, setSelectedAsset } from '../store/asset';
import { ViewTitle } from '../components/ViewTitle';
import { ColumnTitle } from '../components/ColumnTitle';

function AssetsView(props) {
  const dispatch = useDispatch();
  const assets = useSelector(state => state.asset.list);

  useEffect(() => {
    if (assets.length === 0) {
        dispatch(fetchAssets());
    }
  }, [assets.length, dispatch]);

  function onSelect(asset) {
    dispatch(setSelectedAsset(asset));
    props.onSelect();
  }

  return (
    <>
      <ViewTitle>
        Assets
      </ViewTitle>
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <ColumnTitle>
                  Station
                </ColumnTitle>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {assets.map(asset => (
              <TableRow
                key={asset.assetId}
                onClick={() => onSelect(asset)}
                hover
              >
                <TableCell>{asset.name}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </>
  )
}

AssetsView.propTypes = {
  onSelect: PropTypes.func
}

export default AssetsView;