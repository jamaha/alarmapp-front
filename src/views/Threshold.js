import React, { useEffect, useState } from 'react';
import { makeStyles, Dialog, TextField, InputLabel, MenuItem, FormControl, Select, Button, Snackbar, Box, Tooltip, DialogTitle } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { get } from 'lodash';

import * as api from '../api';
import { ViewTitle } from '../components/ViewTitle';
import { fetchThresholds } from '../store/threshold';
import msColors from '../style/mindsphereColors';

const useStyles = makeStyles(theme => ({
  textField: {
    width: '100%'
  },
  formControl: {
    width: '10em'
  },
  form: {
    paddingLeft: '1em',
    paddingRight: '1em'
  }
}));

export default function ThresholdView({ threshold, onSaveDone }) {
  const dispatch = useDispatch();
  const aspectName = useSelector(state => state.aspect.selected.name);
  const variableName = useSelector(state => state.variable.selected.name);
  const assetId = useSelector(state => state.asset.selected.assetId);
  
  const classes = useStyles();
  const [snackbarText, setSnackbarText] = useState('');
  const [saveDone, setSaveDone] = useState(false);

  const defautlValues = {
    entityId: assetId,
    aspectName: aspectName,
    variableName: variableName,
    name: "",
    start: "08:00:00",
    hours: 7,
    active: "ALL",
    expire: "",
    threshold: 0,
    sendTo: "ALL",
    type: "OVER"
  }

  const values = threshold ? { ...threshold } : { ...defautlValues };

  async function saveThresholdAndGoBack() {
    const response = await api.saveThreshold(values);
    if (get(response, 'success')) {
      setSaveDone(true);
      dispatch(fetchThresholds(assetId, aspectName, variableName));
    } else {
      if (get(response, 'message')) {
        setSnackbarText(response.message);
        setTimeout(() => setSnackbarText(''), 5000);
      } else {
        setSnackbarText('Unexpected error');
        setTimeout(() => setSnackbarText(''), 5000);  
      }
    }
  }

  useEffect(() => {
    if (saveDone) {
      setTimeout(onSaveDone, 1800);
    }
  }, [saveDone, onSaveDone])

  return (
    <>
      <ViewTitle>{threshold ? 'Edit threshold' : 'Create Threshold'}</ViewTitle>
      <form noValidate autoComplete="off" className={classes.form}>
        <Tooltip title={tooltips.name}>
          <TextField
            label="Name"
            margin="normal"
            variant="outlined"
            defaultValue={values.name}
            className={classes.textField}
            onChange={e => values.name = e.target.value}
          />
        </Tooltip>
        <Tooltip title={tooltips.startTime}>
          <TextField
            label="Start Time"
            margin="normal"
            type="time"
            defaultValue={values.start}
            inputProps={{
              step: 300, // 5 min
            }}
            variant="outlined"
            onChange={e => values.start = e.target.value}
            className={classes.textField}
          />
        </Tooltip>
        <Tooltip title={tooltips.hour}>
          <TextField
            label="Hours"
            type="number"
            defaultValue={values.hours}
            margin="normal"
            variant="outlined"
            onChange={e => values.hours = Number(e.target.value)}
            className={classes.textField}
          />
        </Tooltip>
        <Tooltip title={tooltips.threshold}>
          <Box display='flex' className={classes.textField}>
            <TextField
              label="Threshold"
              type="number"
              defaultValue={values.threshold}
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
              variant="outlined"
              onChange={e => values.threshold = Number(e.target.value)}
            />
            <FormControl variant="outlined" margin='normal' className={classes.formControl}>
              <InputLabel>
                Type
              </InputLabel>
              <Select
                defaultValue={values.type}
                labelWidth={38}
                onChange={e => values.type = e.target.value}
              >
                <MenuItem value="OVER">OVER</MenuItem>
                <MenuItem value="UNDER">UNDER</MenuItem>
              </Select>
            </FormControl>
          </Box>
        </Tooltip>
        <Button variant="outlined" color="primary" onClick={saveThresholdAndGoBack}>
          Save
        </Button>
      </form>
      <Snackbar
        open={!!snackbarText}
        ContentProps={{style: {
          backgroundColor: msColors.contextRed,
          color: msColors.contextRed500
        }}}
        message={snackbarText}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      />
      <Dialog
        open={saveDone}
        PaperProps={{ style: {
          backgroundColor: msColors.contextGreen50,
          color: msColors.contextGreen600
        }}}
      >
        <DialogTitle>Threshold {threshold ? 'updated' : 'created'}</DialogTitle>
      </Dialog>
    </>
  )
}

const tooltips = {
  name: 'Discriptive name for threshold',
  startTime: 'Time of day when threshold goes active',
  hour: 'After activated, how long threshold should be active',
  threshold: 'At what point threshold should trigger. When value goes OVER or UNDER threshold'
}