export default [
  {
      "temperature": {
          "firsttime": "2019-11-01T11:08:44.133Z",
          "average": 22.681505376344084,
          "countuncertain": 0,
          "maxvalue": 23.1,
          "firstvalue": 23.1,
          "countbad": 0,
          "mintime": "2019-11-01T22:23:48.154Z",
          "lastvalue": 22.56,
          "sum": 2109.38,
          "minvalue": 22.56,
          "maxtime": "2019-11-01T11:08:44.133Z",
          "sd": 0.17115010224355562,
          "lasttime": "2019-11-02T10:53:52.728Z",
          "countgood": 93
      },
      "starttime": "2019-11-01T11:00:00Z",
      "endtime": "2019-11-02T11:00:00Z"
  },
  {
      "temperature": {
          "firsttime": "2019-11-02T11:08:52.739Z",
          "average": 22.42808510638298,
          "countuncertain": 0,
          "maxvalue": 22.56,
          "firstvalue": 22.56,
          "countbad": 0,
          "mintime": "2019-11-03T00:38:57.801Z",
          "lastvalue": 22.36,
          "sum": 2108.24,
          "minvalue": 22.36,
          "maxtime": "2019-11-02T11:08:52.739Z",
          "sd": 0.06718150384841125,
          "lasttime": "2019-11-03T10:54:01.104Z",
          "countgood": 94
      },
      "starttime": "2019-11-02T11:00:00Z",
      "endtime": "2019-11-03T11:00:00Z"
  },
  {
      "temperature": {
          "firsttime": "2019-11-03T11:09:01.199Z",
          "average": 22.203695652173913,
          "countuncertain": 0,
          "maxvalue": 22.36,
          "firstvalue": 22.36,
          "countbad": 0,
          "mintime": "2019-11-04T04:39:07.689Z",
          "lastvalue": 22.24,
          "sum": 2042.74,
          "minvalue": 22.02,
          "maxtime": "2019-11-03T11:09:01.199Z",
          "sd": 0.11119623655768947,
          "lasttime": "2019-11-04T10:54:09.869Z",
          "countgood": 92
      },
      "starttime": "2019-11-03T11:00:00Z",
      "endtime": "2019-11-04T11:00:00Z"
  },
  {
      "temperature": {
          "firsttime": "2019-11-04T11:09:09.989Z",
          "average": 22.174893617021276,
          "countuncertain": 0,
          "maxvalue": 22.38,
          "firstvalue": 22.24,
          "countbad": 0,
          "mintime": "2019-11-05T05:39:16.881Z",
          "lastvalue": 22.1,
          "sum": 2084.44,
          "minvalue": 22,
          "maxtime": "2019-11-04T11:24:10.070Z",
          "sd": 0.09048033383206569,
          "lasttime": "2019-11-05T10:54:18.806Z",
          "countgood": 94
      },
      "starttime": "2019-11-04T11:00:00Z",
      "endtime": "2019-11-05T11:00:00Z"
  },
  {
      "temperature": {
          "firsttime": "2019-11-05T11:09:18.898Z",
          "average": 22.158494623655915,
          "countuncertain": 0,
          "maxvalue": 22.34,
          "firstvalue": 22.1,
          "countbad": 0,
          "mintime": "2019-11-06T05:54:25.225Z",
          "lastvalue": 22.34,
          "sum": 2060.74,
          "minvalue": 21.94,
          "maxtime": "2019-11-06T10:39:26.514Z",
          "sd": 0.08076177948138712,
          "lasttime": "2019-11-06T10:54:26.595Z",
          "countgood": 93
      },
      "starttime": "2019-11-05T11:00:00Z",
      "endtime": "2019-11-06T11:00:00Z"
  },
  {
      "temperature": {
          "firsttime": "2019-11-06T11:09:26.697Z",
          "average": 22.158947368421053,
          "countuncertain": 0,
          "maxvalue": 22.34,
          "firstvalue": 22.34,
          "countbad": 0,
          "mintime": "2019-11-07T06:09:33.512Z",
          "lastvalue": 22.02,
          "sum": 2105.1,
          "minvalue": 21.94,
          "maxtime": "2019-11-06T11:09:26.697Z",
          "sd": 0.10383664989576252,
          "lasttime": "2019-11-07T10:54:35.094Z",
          "countgood": 95
      },
      "starttime": "2019-11-06T11:00:00Z",
      "endtime": "2019-11-07T11:00:00Z"
  },
  {
      "temperature": {
          "firsttime": "2019-11-07T11:09:35.216Z",
          "average": 22.10191489361702,
          "countuncertain": 0,
          "maxvalue": 22.24,
          "firstvalue": 22.02,
          "countbad": 0,
          "mintime": "2019-11-08T06:24:42.377Z",
          "lastvalue": 22.04,
          "sum": 2077.58,
          "minvalue": 21.98,
          "maxtime": "2019-11-07T20:54:38.889Z",
          "sd": 0.07828312765876368,
          "lasttime": "2019-11-08T10:54:43.969Z",
          "countgood": 94
      },
      "starttime": "2019-11-07T11:00:00Z",
      "endtime": "2019-11-08T11:00:00Z"
  },
  {
      "temperature": {
          "firsttime": "2019-11-08T11:09:44.011Z",
          "average": 22.144719101123595,
          "countuncertain": 0,
          "maxvalue": 22.28,
          "firstvalue": 22.04,
          "countbad": 0,
          "mintime": "2019-11-08T14:09:45.072Z",
          "lastvalue": 22.14,
          "sum": 1970.88,
          "minvalue": 22.02,
          "maxtime": "2019-11-09T01:24:48.866Z",
          "sd": 0.051496479374511704,
          "lasttime": "2019-11-09T10:54:52.047Z",
          "countgood": 89
      },
      "starttime": "2019-11-08T11:00:00Z",
      "endtime": "2019-11-09T11:00:00Z"
  }
]